/**
 * @author Charlie Calvert
 */

if ( typeof define !== 'function') {
	var define = require('amdefine')(module);
}

define(['DefaultReader', 'JsonReader', "MarkdownReader"], function(defaultReader, jsonReader, markdownReader) {'use strict';

	function Factory02() {
	}


	Factory02.prototype.readerObject = defaultReader;

	Factory02.prototype.create = function(options) {

		switch (options.objectType) {
			case "JsonReader":
				this.readerObject = jsonReader;
				break;
			case "MarkdownReader":
				this.readerObject = markdownReader;
				break;
			case "DefaultReader":
				this.readerObject = defaultReader;
				break;
			default:
				this.readerObject = defaultReader;
		}

		return new this.readerObject(options);

	};

	return Factory02;
}); 