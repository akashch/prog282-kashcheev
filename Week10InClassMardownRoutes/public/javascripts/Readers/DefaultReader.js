/**
 * @author Charlie Calvert
 */

define(function(require) {'use strict';

	var DefaultReader = ( function() {

			function DefaultReader() {

			}


			DefaultReader.prototype.readFile = function() {
				return "I'm the default reader";
			};

			DefaultReader.prototype.display = function() {
				var data = this.readFile();
				$("#display").text(data);
			};

			return DefaultReader;
		}());

	return DefaultReader;

}); 