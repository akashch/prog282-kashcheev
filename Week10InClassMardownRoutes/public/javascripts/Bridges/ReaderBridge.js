/**
 * @author Charlie Calvert
 */

define(function(require) {
	'use strict';

	var Readers = (function() {

		function Readers(reader) {
			this.setReader(reader);
		}

		Readers.prototype.setReader = function(reader) {
			this.reader = reader;
		};

		Readers.prototype.readFile = function(fileName, customCallback) {
			return this.reader.readFile(fileName, customCallback);
		};

		Readers.prototype.display = function() {
			return this.reader.display();
		};

		return Readers;
	}());

	return Readers;
});
