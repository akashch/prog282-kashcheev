/**
 * @author Charlie Calvert
 */

require.config({
	paths : {
		"jquery" : "jquery-2.1.1",
		"DefaultReader" : "./Readers/DefaultReader",
		"JsonReader" : "./Readers/JsonReader",
		"MarkdownReader" : "./Readers/MarkdownReader",
		"ReaderFactory" : "./Factories/ReaderFactory",
		"BridgeFactory" : "./Factories/BridgeFactory",
		"ReaderBridge" : "./Bridges/ReaderBridge",
		"FancyReaderBridge" : "./Bridges/FancyReaderBridge",
		"DisplayAddress" : "./Display/DisplayAddress",
		"DisplayFileList": "./Display/DisplayFileList",
		"Markdown" : "./Markdown/Converter",
		"Editor" : "./Markdown/Editor"
	}
});

function endsWith(value, suffix) {
    return value.indexOf(suffix, this.length - suffix.length) !== -1;
}

require([ 'jquery', "Control", "MarkShow", "TinyPubSub" ],
function(jq, Control, MarkShow, PubSub) {
    'use strict';
    console.log("Main called");

    $(document).ready(function() {
        if (endsWith(document.URL, "Markdown")) {
            var markShow = new MarkShow();
            markShow.getPick();
        } else {
            var control = new Control();
        };
    });
});