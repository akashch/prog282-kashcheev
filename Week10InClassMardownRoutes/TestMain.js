/**
 * @author Charlie Calvert
 */

var tests = [];
for (var file in window.__karma__.files) {
    //console.log(file);
    if (/Tests\.js$/.test(file)) {
        console.log("Testing: " + file);
        tests.push(file);
    }
}

requirejs.config({
    baseUrl : '/base',
    paths : {
    	'jquery': 'public/javascripts/jquery-2.1.0.min',
    	'BridgeFactory' : 'public/javascripts/Factories/BridgeFactory',
        'ReaderFactory' : 'public/javascripts/Factories/ReaderFactory',
        'ReaderBridge': 'public/javascripts/Bridges/ReaderBridge',
        'FancyReaderBridge': 'public/javascripts/Bridges/FancyReaderBridge',
        'DefaultReader': 'public/javascripts/Readers/DefaultReader',
        'JsonReader': 'public/javascripts/Readers/JsonReader',
        'MarkdownReader': 'public/javascripts/Readers/MarkdownReader',
        'DisplayAddress': 'public/javascripts/Display/DisplayAddress',
        'DisplayFileList': 'public/javascripts/Display/DisplayFileList',
        'Utilities' : 'public/javascripts/Utilities'
    },
    shim : {
    },
    deps : tests,
    callback : window.__karma__.start
});
