/**
 * @author Charlie Calvert
 */

var tests = [];
for (var file in window.__karma__.files) {    
    if (/Spec\.js$/.test(file)) {
        console.log("Testing: " + file);
        tests.push(file);
    }
}
requirejs.config({
    baseUrl : '/base',
    paths : {
		'jquery' : 'public/javascripts/jquery-2.1.0.min',        
        'FancyReader' : 'public/javascripts/FancyReader',
        'DisplayReaders' : 'public/javascripts/DisplayReaders',
        'Reader' : 'public/javascripts/Reader',
        'TheFactory' : 'public/javascripts/TheFactory',
        'JsonReader': 'public/javascripts/readers/JsonReader',      
        'MarkdownReader': 'public/javascripts/readers/MarkdownReader'
    },
    shim : {
    },
    deps : tests,
    callback : window.__karma__.start
});
