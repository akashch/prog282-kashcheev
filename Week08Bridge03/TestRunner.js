var express = require('express');
var app = express();
var fs = require('fs');

var logger = require('morgan');
var favicon = require('static-favicon');

var routes = require('./routes/index');
var users = require('./routes/users');

app.use(logger('dev'));
app.use(favicon());
app.use('/users', users);
var port = process.env.PORT || 30025;

app.get('/', function(request, response) {'use strict';
	var html = fs.readFileSync(__dirname + '/tests/ReadersSpec.html');
	response.writeHeader(200, {"Content-Type": "text/html"});   
	response.write(html);
	response.end();
});


app.use("/", express.static(__dirname + '/tests'));
app.use("/", express.static(__dirname + '/public'));

app.listen(port);
console.log('Listening on port :' + port);


