/**
 * @author Andrey Kashcheev
 */

require.config({
    paths : {
        'FancyReader' : '/javascripts/FancyReader',
        'DisplayReaders' : '/javascripts/DisplayReaders',
        'Reader' : '/javascripts/Reader',
        'Factory' : '/javascripts/TheFactory',
        'JsonReader': '/javascripts/readers/JsonReader',      
        'MarkdownReader': '/javascripts/readers/MarkdownReader',
        'JQuery' : '/javascripts/jquery-2.1.0.min',
        'jasmine' : 'jasmine-2.0.0/jasmine',
        'jasmine-html' : 'jasmine-2.0.0/jasmine-html',
        'boot' : 'jasmine-2.0.0/boot'
    },
    shim : {
        'jasmine' : {
            exports : 'jasmine'
        },
        'jasmine-html' : {
            deps : ['jasmine'],
            exports : 'jasmine'
        },
        'boot' : {
            deps : ['jasmine', 'jasmine-html'],
            exports : 'jasmine'
        }
    }
});

/*
 * Do this two step dance with two requires 
 * when you want to load jasmine.
 */ 
require(['boot'], function(boot) {
    'use strict';
    
    // Load the specs with second call to require
    require(["ReadersSpec"], function() {
        console.log("Main called.");    
        window.onload();
    });
});

