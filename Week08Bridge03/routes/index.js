var express = require('express');
var fs = require('fs');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {'use strict';
  res.render('index', { title: 'Week04Readers' });
});



module.exports = router;
