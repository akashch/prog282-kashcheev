/**
 * @author Andrey Kashcheev
 */
var express = require('express');
var router = express.Router();
var queryMongo = require('../public/javascripts/QueryMongo').QueryMongo;
var fs = require('fs');



/* GET home page. */
router.get('/', function(request, response) {
	console.log("called");//debug
    response.render('MarkdownEditor', {
        title : 'Week08 - Markdown Editor'
    });
});

router.get('/storeInDb', function(request, response) {
	console.log("storeInDb called");//debug
	queryMongo.insertIntoCollection(response, 'test_insert', request.query);
});

router.get('/storeOnDisc', function(request, response) {
	console.log("storeOnDisc called");//debug
	fs.writeFile("/home/andrey/gitHub/akashch.github.io/" + request.query.fileName + ".md", request.query.markdown, function(err) {
	    if(err) {
	        console.log(err);
	    } else {
	        console.log("The file was saved!");
	    }
	});
});



module.exports = router;
