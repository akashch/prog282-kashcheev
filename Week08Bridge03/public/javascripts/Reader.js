/**
 * @author Andrey Kashcheev
 */

define(function(require) {'use strict';

	var Reader = (function() {


		function Reader(reader){
			this.setReader(reader);
		}
				
		Reader.prototype.setReader = function(reader){
			this.reader = reader;
		};

		Reader.prototype.read = function(path, file, callback){
			this.reader.read(path, file, function(data){
				callback(data);
			});
		};
		
		Reader.prototype.write = function(){
			return this.reader.write();
		};

		Reader.prototype.remove = function(){
			return this.reader.remove();	
		};
		
		Reader.prototype.getPath = function(){
			return this.reader.path;
		};
		
		Reader.prototype.setPath = function(path){
			this.reader.path = path;
		};
		
		return Reader;
	}());

	return Reader;
});