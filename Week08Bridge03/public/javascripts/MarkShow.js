/**
 * @author Andrey Kashcheev
 */

define(["PagedownSetup", "Markdown", "Editor"], 
    function(PagedownSetup, Markdown, Editor) {
    
    
    var MarkShow = (function() {

        var saveMarkdown;
        var converter;
        var data  = null;

        function MarkShow(myReaders) {
        	data = localStorage.getItem("content");
            var pagedownSetup = new PagedownSetup();
            converter = pagedownSetup.setupConverter(Markdown);
            var inputText = $("#wmd-input-elf");
            inputText.html(JSON.parse(data).markdown.text);
            
            converter.hooks.chain("preConversion", function(text) {
                console.log(text);
                saveMarkdown = text;
                return text;
            });
            
            $('#saveDb').click(function(){
            	var tempJson = {"fileName":JSON.parse(data).fileName, "path":JSON.parse(data).path, "markdown": saveMarkdown};
            	console.log(tempJson);
            	$.getJSON( "/MarkdownEditor/storeInDb", tempJson, function(data) {
					console.log(data);
				});
            });
            
            
            $('#saveDisc').click(function(){
            	var tempJson = {"fileName":JSON.parse(data).fileName, "path":JSON.parse(data).path, "markdown": saveMarkdown};
            	$.getJSON( "/MarkdownEditor/storeOnDisc", tempJson, function(data) {
					console.log(data);
				});
            });
        }

        return MarkShow;
        
    }());
    
    return MarkShow;
});