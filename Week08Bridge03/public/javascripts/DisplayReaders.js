/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
	var define = require('amdefine')(module);
}

define(function(require) { 'use strict';
	var DisplayReaders = ( function() { 
		
		var listItem = null;
		var theReaders = null;
		var myJson = null;
		var route = null;
		var name = null;
		
		function DisplayReaders(myReaders){
			theReaders = myReaders;
			$('#readJson').click(function(){
				myReaders.myJsonReader.setPath(null); //Part01
				route = "Presidents.json";
				read({"path" : "readJson"}, {"name": route}, myReaders.myJsonReader);
			});
			$('#readMark').click(function(){
				myReaders.myJsonReader.setPath(null); //Part01
				route = "Simple.md";
				read({"path" : "readMarkdown"}, {"name": route}, myReaders.myMarkdownReader);
			});
			$('#showLength').click(function(){
				clear();
				route = "Presidents.json";
				myReaders.myFancyReader.length({"path" : "readJson"},{"name":route}, function(data){
					$("#content").append("<li data=" + route + ">" + data.text.length + "</li>");
					$("ul li").addClass("listItem");
					listItem = $(".listItem");
					$(listItem).click(reaload);
				});
			});
			myReaders.myJsonReader.setPath("fileList");
			route = "FileList.json";
			read({"path" : "readConfig"}, {"name":route},myReaders.myJsonReader);
		}
		
		var listClick = function(){
			name = (event.target.innerText);
			route = search(name).Path;
			switch(route.slice(route.indexOf('.')+1)){
				case "json": read({"path" : "readJson"}, {"name": route}, theReaders.myJsonReader);
					break;
				case "md" : read({"path" : "readMarkdown"}, {"name": route}, theReaders.myMarkdownReader);
					break;
			}
		};
		
		var search = function(name){
			var arr = JSON.parse(myJson);
			var length = arr.length;
			for(var i = 0; i < length; i++){ 
				if(arr[i].Name.slice(0, arr[i].Name.indexOf(".")) === name){
					return arr[i];
				}
			}
		};
		
		var clear = function() {
			$("#content").empty();
		};
		
		var reaload = function(){
			clear();
			route = "FileList.json";
			read({"path" : "readConfig"}, {"name": route},theReaders.myJsonReader);
		};
		
		var show = function(data) {
			console.log(data);
			var temp = null;
			var i = null;
			switch(data.type){
				case "jsonConfig": temp = JSON.parse(data.text);
									for(i  = 0; i < temp.length; i++){
											$("#content").append("<li data=" + route + ">" + temp[i].Name.slice(0, temp[i].Name.indexOf(".")) + "</li>");
											$("ul li").addClass("listItem");
											listItem = $(".listItem");
									}
									$(listItem).click(listClick);
					break;
				case "JSON"     : temp = JSON.parse(data.text);
								for(i = 0; i < temp.length; i++){
									$("#content").append("<li data=" + route + ">" + temp[i].product + " " + temp[i].company + "</li>");
									$("ul li").addClass("listItem");
									listItem = $(".listItem");
								}
								$(listItem).click(reaload);
					break;
				case "Markdown"	:	//$("#content").append("<li data=" + route + ">" + data.text + "</li>");
									//console.log("METKA");
									//$("ul li").addClass("listItem");
									//listItem = $(".listItem");
									//$(listItem).click(reaload);
									//localStorage.setItem('markdown', JSON.stringify(data));
									localStorage.setItem('content', JSON.stringify({"fileName": name, "path": route, "markdown": data}));
									window.location.href = '/MarkdownEditor';
									
					break;
				}
		};

		var read = function(path, file, reader){
			clear();
			reader.read(path, file, function(data){
				if(data.type === "jsonConfig") myJson = data.text;
				show(data);
			});
		};
		
		return DisplayReaders;
	})();
	return DisplayReaders;
});