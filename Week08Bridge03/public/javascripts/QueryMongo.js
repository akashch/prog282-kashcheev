/**
 * @author Andrey Kashcheev
 */
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var fs = require('fs');
var exec = require('child_process').exec;
var assert = require('assert');

var QueryMongo = (function() {'use strict';

	var response = null;
	var database = null;
	var url = null;
	//var collectionName = 'Config';
	/*
	 * Normally we do not close the database. If you have more
	 * more than one MongoClient then call it, otherwise, don't 
	 * call it. So we default to false.
	 */
	var callClose = false;
	var theUrl;
	function QueryMongo() {
		message("QueryMongo construtor called");
		fs.readFile('./config.json', 'utf8', function (err, data) {
					if (err) {
						console.log('Error: ' + err);
					}
					data = JSON.parse(data);
					theUrl = data.urls[0];
					message("mongoAccess - readCongfig - URL -> " + theUrl);
				});
	}
	
	function message(printMessage) {
        console.log("****************************************************************************");
        console.log(printMessage);
        console.log("****************************************************************************");
    }
    
    QueryMongo.prototype.test = function (){
    	console.log("WHATS UP! ");
    };
	
	//Inserting 
	QueryMongo.prototype.insertIntoCollection = function(response, collectionName,  objectToInsert){
		message("QueryMongo insertIntoCollection called");
		message(objectToInsert);
		getDatabase(function getCol(database) {
			var collection = database.collection(collectionName);
			collection.insert(objectToInsert, function(err, docs) {
				if (err) {
					throw err;
				}
				if (callClose) { closeDatabase(); }
				message("insert succeeded");
				response.send({ result: "Success", mongoDocument: docs });
			});
		});
	};
	
	//Getting data from MongoDB
	QueryMongo.prototype.readDB = function(request,collectionName, response) {
			message("QueryMongo - readDB called");
			getDatabase(function getCol(database) {
				var collection = database.collection(collectionName);
					collection.find().toArray(function(err, theArray) {
						database.close();
						response.send(theArray);
					});
			});
	};
	
	//Remove all from db
	QueryMongo.prototype.removeAll = function(collectionName) {
        console.log("QueryMongo.removeAll called");
			getDatabase(function getCol(database) {
				var collection = database.collection(collectionName);
				collection.remove(function(err, data) {
                    if (err) {
                        throw err;
                    }               
                    console.log("Item deleted");
                });
			});  
    };
	

	//Establishing connection with MongoDB
	var getDatabase = function(func) {
			message('QueryMongo - Called getDatabase');
				message('QueryMongo - Querying for database');
					MongoClient.connect(theUrl, function(err, databaseResult) {
						if (err) {
							throw err;
						}
						database = databaseResult;
						func(database);
					});
	};


	return QueryMongo;

})();


exports.QueryMongo = new QueryMongo();
