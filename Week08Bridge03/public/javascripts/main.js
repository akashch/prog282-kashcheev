/**
 * @author Andrey Kashcheev
 */

require.config({
	paths : {
		"jquery" : 'jquery-2.1.0.min',
		"JsonReader" : 'readers/JsonReader',
		"MarkdownReader" : 'readers/MarkdownReader',
		"Markdown" : "Markdown/Converter",
		"Editor" : "Markdown/Editor",
		"MarkShow" : "MarkShow",
		"QueryMongo" : "QueryMongo",
		"wmd-buttons" : "images/wmd-buttons"
	}
});

require(['jquery', 'Reader', 'FancyReader', 'JsonReader', 'MarkdownReader', 'TheFactory', 'DisplayReaders', 'MarkShow'], function(jq, Reader, FancyReader, JsonReader, MarkdownReader, Factory, DisplayReaders, MarkShow) {'use strict';

	$(document).ready(function() {

		var myFactory = new Factory();

		var jsonReader = myFactory.createReader({
			path : null, // to do
			type : "JsonReader"
		});

		var myReaders = {
			myJsonReader : myFactory.createBridge("Reader", jsonReader),
			myMarkdownReader : myFactory.createBridge("Reader", myFactory.createReader({
				path : null, //to do
				type : "MarkdownReader"
			})),
			myFancyReader : myFactory.createBridge("FancyReader", jsonReader),
			data : null
		};

		if (document.URL === "http://localhost:30025/MarkdownEditor") {
			var showMark = new MarkShow(myReaders);
		} else {
			var display = DisplayReaders(myReaders);
		}
	});
});
