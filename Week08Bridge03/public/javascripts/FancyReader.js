/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(["Reader"], function(Reader) {'use strict';
	
	var FancyReader = (function() {
	
		function FancyReader(reader){
			this.setReader(reader);
		}
		
		FancyReader.prototype = new Reader();
		
		FancyReader.prototype.length = function(path, file, callback){
			this.reader.read(path,file, function(data){
				callback(data);
			});
		};
		
        return FancyReader;
    }());
    
    

    return FancyReader;
}); 