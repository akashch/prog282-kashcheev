/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {'use strict';

    var JsonReader = function(options) {
		this.path = options.path;
		this.type = "JsonReader";
	};

    JsonReader.prototype.read = function(path, file, callback){
		this.path = path.path;
		$.getJSON( "/users/" + this.path, file, function(data) {
			callback(data);
		});
    };

	JsonReader.prototype.write = function(){
		return "JSON has been successfully written";
	};

    JsonReader.prototype.remove = function(){
		return "JSON has been successfully deleted";
	};

    return JsonReader;

}); 