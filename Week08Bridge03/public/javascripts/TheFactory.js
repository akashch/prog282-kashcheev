/**
 * @author Andrey Kashcheev
 */
if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {
	'use strict';
	//Readers
	var jsonReader = require("JsonReader");
	var markdownReader = require("MarkdownReader");
	
	//Bridges
	var readers = require("Reader");
	var fancyReaders = require("FancyReader");

	function TheFactory(){}
	
	TheFactory.prototype.readerType = null;
	TheFactory.prototype.bridgeType = null;
	
	TheFactory.prototype.createReader = function(options){
		switch(options.type){
			case 'JsonReader': this.readerType = jsonReader;
				break;
			case 'MarkdownReader': this.readerType = markdownReader;
				break;
			default: this.readerType = jsonReader;
		}
		
		return new this.readerType(options);
	};
	
	
	TheFactory.prototype.createBridge = function(type,readerType){
		switch(type){
			case "Reader" : this.bridgeType = readers;
				break;
			case "FancyReader" : this.bridgeType = fancyReaders;
				break;
			default: this.bridgeType = readers;
		}
		return new this.bridgeType(readerType);
	};

    return TheFactory;
});