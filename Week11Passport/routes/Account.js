var express = require('express');
var router = express.Router();
var passport = require('passport');
var GoogleStrategy = require('passport-google').Strategy;
var signedIn = require("./SignedIn").signedIn;

/* GET home page. */
router.get('/', signedIn, function(request, response) {
    console.log("Index called");
    response.render('Account', { title: 'Passport Account' });
    
    //response.render('Account', { user : request.user });
});



module.exports = router;

