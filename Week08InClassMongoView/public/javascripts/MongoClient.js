/**
 * @author Andrey Kashcheev
 */
define(function() {
    
    var MongoClient = (function() {

        function MongoClient() {
			$("#mongoData").click(function(){
				$.getJSON( "/readAll", function(data) {
				console.log(data);
					$("#result").text(JSON.stringify(data));
				});
			});
        }

        return MongoClient;

    }());

    return MongoClient;

});