var MyObject = (function() {

    function MyObject() {
		console.log("Constructor calleds");
		$("#sendString").click(this.readyCalled);
    }
    
    MyObject.prototype.readyCalled = function() {
        $("#readyCalled").html("Click event was handeled and text added to the page");
    }
    return MyObject;
}());


$(document).ready(function() {
    var myObject = new MyObject();
});
