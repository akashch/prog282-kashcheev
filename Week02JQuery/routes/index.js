var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express', content : "PROG282-Week02-Express" });
});

module.exports = router;
