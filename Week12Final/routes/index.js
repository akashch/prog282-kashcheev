var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res) {
    'use_strict';
    res.render('index', {
        title: 'Week10BridgePictureCaption'
    });
});

router.get('/read', function(request, response) {
    'use_strict';
    console.log('root read called: ' + JSON.stringify(request.query));
    var fileName = request.query.fileName;
    fs.readFile(fileName, 'utf8', function(error, data) {
        console.log(fileName);
        if (error) {
            console.log("ERRROR");
            response.send({
                "Could_Not_Find_File": error,
                fileName: fileName
            });
            return;
        }

        try {
            var jsonObject = JSON.parse(data);
            console.log("Sending error");
            console.log(jsonObject);
            response.send(jsonObject);
        } catch (e) {
            console.log("Sending error2");
            response.send({
                "error": "Could not parse",
                "Could_Not_Parse_JSON": "error"
            });
        }
    });

});

module.exports = router;
