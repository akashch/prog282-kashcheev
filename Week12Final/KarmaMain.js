/**
 * @author Charlie Calvert
 */

var tests = [];
for (var file in window.__karma__.files) {
    if (/Tests\.js$/.test(file)) {
        console.log("Testing: " + file);
        tests.push(file);
    }
}
requirejs.config({
    baseUrl: '/base',
    paths: {
        "jquery": "public/javascripts/jquery-2.1.1",
        "Control": "public/javascripts/Control",
        "PubSub": "public/javascripts/TinyPubSub",
        "DisplayFactory": "public/javascripts/Factories/DisplayFactory",
        "DisplayAddress": "public/javascripts/Display/DisplayAddress",
        "DisplayFileList": "public/javascripts/Display/DisplayFileList",
        "DisplayPictureCaption": "public/javascripts/Display/DisplayPictureCaption",
        "ReaderFactory": "public/javascripts/Factories/ReaderFactory",
        "BridgeFactory": "public/javascripts/Factories/BridgeFactory",
        "JsonReader": "public/javascripts/Readers/JsonReader",
        "DefaultReader": "public/javascripts/Readers/DefaultReader",
        "MarkdownReader": "public/javascripts/Readers/MarkdownReader",
        "FancyReaderBridge": "public/javascripts/Bridges/FancyReaderBridge",
        "ReaderBridge": "public/javascripts/Bridges/ReaderBridge",
        "Utilities": "public/javascripts/Utilities",
    },
    shim: {},
    deps: tests,
    callback: window.__karma__.start
});
