require.config({
    paths: {
        "jquery": "jquery-2.1.1",
        "PubSub": "TinyPubSub",
        "ReaderFactory": "Factories/ReaderFactory",
        "DisplayFactory": "Factories/DisplayFactory",
        "DisplayAddress": "Display/DisplayAddress",
        "DisplayFileList": "Display/DisplayFileList",
        "DisplayPictureCaption": "Display/DisplayPictureCaption",
        "BridgeFactory": "Factories/BridgeFactory",
        "JsonReader": "Readers/JsonReader",
        "DefaultReader": "Readers/DefaultReader",
        "MarkdownReader": "Readers/MarkdownReader",
        "FancyReaderBridge": "Bridges/FancyReaderBridge",
        "ReaderBridge": "Bridges/ReaderBridge"
    }
});

require(['jquery', "Control", "PubSub"],

    function(jq, Control, PubSub) {
        'use strict';
        console.log("Main called");

        $(document).ready(function() {
            var control = new Control();
        });
    }

);
