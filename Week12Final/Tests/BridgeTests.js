/**
 * @author Andrey Kashcheev
 */

define(["Control", "JsonReader", "FancyReaderBridge", "ReaderBridge", "BridgeFactory"],
    function(Control, JsonReader, FancyReaderBridge, ReaderBridge, BridgeFactory) {
        'use strict';

        describe("Bridge Tests", function() {

            it("proves we can run a test", function() {
                expect(true).toBe(true);
            });

            it("proves we can create Control", function() {
                var control = new Control();
                expect(control).toBeTruthy();
            });

            it("proves we can create a ReaderBridge", function() {
                var readerBridge = new ReaderBridge();
                expect(readerBridge).toBeTruthy();
            });


            it("proves we can create a FancyReaderBridge", function() {
                var fancyReaderBridge = new FancyReaderBridge();
                expect(fancyReaderBridge).toBeTruthy();
            });

            it("proves we can create a BridgeFactory", function() {
                var bridgeFactory = new BridgeFactory();
                expect(bridgeFactory).toBeTruthy();
            });

            it("proves we can create a JsonReader", function() {
                var jsonReader = new JsonReader();
                expect(jsonReader).toBeTruthy();
            });




            it("proves we can create a JsonReader and read something", function(done) {
                var jsonReader = new JsonReader();
                jsonReader.readFile("public/FileList.json", function(dataFromServer) {
                    expect(dataFromServer.content["Presidents01.json"]).toBe("/home/andrey/workspace/Week10BridgePictureCaption/data/Presidents01.json");
                    done();
                });
            });

        });
    });
