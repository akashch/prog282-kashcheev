var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/readJson', function(request, response) {'use strict';
	console.log(request.query.name);
	var data = fs.readFileSync(request.query.name, 'utf8');
	response.send({"type":"JSON", "text": data});
});


router.get('/readConfig', function(request, response) {'use strict';
	var data = fs.readFileSync(request.query.name, 'utf8');
	response.send({"type":"jsonConfig", "text": data});
});



router.get('/readMarkdown',function(request, response) {'use strict';
	console.log(request.query.name);
	var data = fs.readFileSync(request.query.name, 'utf8');
	response.send({"type":"Markdown", "text":data});
});

module.exports = router;
