/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([ "JQuery","Factory","Reader", "FancyReader", "JsonReader", "MarkdownReader",  "DisplayReaders"], 
		function( jq, Factory, Reader, FancyReader, JsonReader, MarkdownReader, DisplayReaders) {
			'use strict'; 

    describe("Week04BridgeReader Tests Suite", function() {
		'use strict';
		
        it("proves we can run a test", function() {
            expect(true).toBe(true);
        }); 
        
        describe("Factory Tests Suite", function() {
        	'use strict';
        	
        	var myFactory = null;
        	
        	beforeEach(function(){
				myFactory = new Factory();
			});

        	it("proves that Factory has method createReader", function() {
				expect(Factory.prototype.hasOwnProperty('createReader')).toBe(true);
        	}); 
        	
        	
        	it("proves that Factory has method createBridge", function() {
				expect(Factory.prototype.hasOwnProperty('createBridge')).toBe(true);
        	}); 
        	
        	it("proves that Factory has property readerType", function() {
				expect(Factory.prototype.hasOwnProperty('readerType')).toBe(true);
        	}); 
        	
        	it("proves that Factory has property bridgeType", function() {
				expect(Factory.prototype.hasOwnProperty('bridgeType')).toBe(true);
        	}); 
        	
        	it("tests that myFactory object has been created and is an instace of Factory", function() {
				expect(typeof myFactory === "object").toBe(true);
            	expect(myFactory instanceof Factory).toBe(true);
        	}); 
        	
        	
			it("tests that myFactory object has been created and is an instace of Factory", function() {
				expect(typeof myFactory === "object").toBe(true);
            	expect(myFactory instanceof Factory).toBe(true);
        	}); 
        	
        	it("tests that myFactory creates JSONReader MarkdownReader when a specific type passed ", function() {
        			var jsonReader = myFactory.createReader({
											path : null, // to do
			        						type : "JsonReader"
    									});
    									
    				var markdownReader = myFactory.createReader({
												    path: null, //to do
												    type : "MarkdownReader"
    											});
            	expect(jsonReader instanceof JsonReader).toBe(true);
            	expect(markdownReader instanceof MarkdownReader).toBe(true);
        	}); 
        	 	
        	it("tests that myFactory creates correct Bridges when a specific type passed ", function() {
        		    var jsonReader = myFactory.createReader({
																path : null, // to do
								        						type : "JsonReader"
    														});
        			var myReaders = {
									myJsonReader : myFactory.createBridge("Reader",jsonReader),
									myMarkdownReader : myFactory.createBridge("Reader", myFactory.createReader({
																    	path: null, //to do
																    	type : "MarkdownReader"
    																})),
									myFancyReader : myFactory.createBridge("FancyReader",jsonReader)
									};
            	expect(myReaders.myJsonReader instanceof Reader).toBe(true);
            	expect(myReaders.myMarkdownReader instanceof Reader).toBe(true);
            	expect(myReaders.myFancyReader instanceof FancyReader).toBe(true);
        	});
        	
        	it("tests that myFactory creates correct default Reader", function() {
        		var defaultReader = myFactory.createReader({
															path : null, // to do
								        					type : "default"
    													});    		
        		
            	expect(defaultReader instanceof JsonReader).toBe(true);
        	}); 
        	
        	it("proves that defaultReader has read method", function() {
        		var defaultReader = myFactory.createReader({
													path : null, // to do
								        			type : "default"
    													});   
				expect(JsonReader.prototype.hasOwnProperty("read")).toBe(true);
	        });
        	
        	
        	it("tests that myFactory creates correct default BridgeReader", function() {
        		var defaultReader = myFactory.createReader({
															path : null, // to do
								        					type : "default"
    													});
        		var myReaders = {
									myDefaultReader : myFactory.createBridge("Reader",defaultReader)
								}; 
            	expect(myReaders.myDefaultReader instanceof Reader).toBe(true);
        	}); 
   		});
   		
   		describe("Bridge Tests Suite", function() {
        	'use strict';
        	var myFactory = null,
        		jsonReader = null,
        		myReaders = null;
        		
        	beforeEach(function(){
				myFactory = new Factory();
				jsonReader = myFactory.createReader({
															path : null, // to do
								        					type : "JsonReader"
    													});
        		myReaders = {
									myJsonReader : myFactory.createBridge("Reader",jsonReader),
									myMarkdownReader : myFactory.createBridge("Reader", myFactory.createReader({
																    	path: null, //to do
																    	type : "MarkdownReader"
    																})),
									myFancyReader : myFactory.createBridge("FancyReader",jsonReader)
									};
			});
        	
			it("proves that Reader has method setReader", function() {
            	expect(Reader.prototype.hasOwnProperty('setReader')).toBe(true);
        	}); 
        	
        	it("proves that Reader has method read", function() {
            	expect(Reader.prototype.hasOwnProperty('read')).toBe(true);
        	}); 
        	
        	it("proves that FancyReader has been inhernted from Reader", function() {
            	expect(myReaders.myFancyReader instanceof Reader).toBe(true);
            	expect(myReaders.myFancyReader instanceof FancyReader).toBe(true);
        	});
        	
        	it("proves that FancyReader has method lenght", function() {
            	expect(FancyReader.prototype.hasOwnProperty('length')).toBe(true);
        	});
        });
   		
   		describe("Tests Suite for JSONReader", function() {
   			
   			var myFactory = null;
   			var jsonReader = null;
   			
   			beforeEach(function(){
				myFactory = new Factory();
				jsonReader = myFactory.createReader({
											path : null, // to do
			        						type : "JsonReader"
    									});
			});
          	
          	it("proves that config JSON has been read by Server and sent back to client", function(done) {
    			jsonReader.read({"path" : "readConfig"}, {"name":"FileList.json"}, function(data){
    				expect(data.type).toEqual("jsonConfig");
    				done();
    			});
          	});
          	
          	it("proves that Presidents.json has been read by Server and sent back to client", function(done) {
    			jsonReader.read({"path" : "readJson"}, {"name":"Presidents.json"}, function(data){
    				expect(data.type).toEqual("JSON");
    				done();
    			});
          	});
          	
          	it("proves that Databases.json has been recieved from Server", function(done) {
    			jsonReader.read({"path" : "readJson"}, {"name":"data/Databases.json"}, function(data){
    				expect(data.type).toEqual("JSON");
    				done();
    			});
          	});
          	
          	
          	it("proves that Frameworks.json has been recieved from Server", function(done) {
    			jsonReader.read({"path" : "readJson"}, {"name":"data/Frameworks.json"}, function(data){
    				expect(data.type).toEqual("JSON");
    				done();
    			});
          	});
          	
          	it("proves that OperationSystems.json has been recieved from Server", function(done) {
    			jsonReader.read({"path" : "readJson"}, {"name":"data/OperationSystems.json"}, function(data){
    				expect(data.type).toEqual("JSON");
    				done();
    			});
          	});
          	
          	it("proves that JsonReader has read method", function() {
				expect(JsonReader.prototype.hasOwnProperty("read")).toBe(true);
          	});
          	
   		});
   		
   		describe("Tests Suite for MarkdownReader", function() {	
   			var markdownReader = null;
   			var myFactory = null;
   			beforeEach(function(){
				myFactory = new Factory();
		
    			markdownReader = myFactory.createReader({
											path: null, //to do
											type : "MarkdownReader"
    									});
			});
			
			it("proves that Markdown has been read by Server and sent back to client", function(done) {
    			markdownReader.read({"path" : "readMarkdown"}, {"name": "Simple.md"}, function(data){
    				expect(data.type).toEqual("Markdown");
    				done();
    			});
          	});
          	
          	it("proves that MarkdownReader has read method", function() {
				expect(MarkdownReader.prototype.hasOwnProperty("read")).toBe(true);
          	});
			
   			
   		});
    });
});