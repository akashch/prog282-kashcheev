/**
 * @author Andrey Kashcheev
 */

require.config({
    paths : {
		"jquery" : 'jquery-2.1.0.min',
        "JsonReader" : 'readers/JsonReader',
        "MarkdownReader" : 'readers/MarkdownReader'
    }
});

require(['jquery', 'Reader', 'FancyReader', 'JsonReader', 'MarkdownReader', 'TheFactory', 'DisplayReaders'], 
    function(jq, Reader, FancyReader, JsonReader, MarkdownReader, Factory, DisplayReaders) {'use strict';
    
	var myFactory = new Factory();
	
	var jsonReader = myFactory.createReader({
		path : null, // to do
        type : "JsonReader"
    });

	var myReaders = {
		myJsonReader : myFactory.createBridge("Reader",jsonReader),
		myMarkdownReader : myFactory.createBridge("Reader", myFactory.createReader({
			path: null, //to do
			type : "MarkdownReader"
		})),
		myFancyReader : myFactory.createBridge("FancyReader",jsonReader)
	};

    var display = DisplayReaders(myReaders);
    
});
