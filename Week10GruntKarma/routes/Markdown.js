/**
 * @author Andrey Kashcheev
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    'use_strict';
    console.log("Markdown called");
    res.render('Markdown', {
        title: 'Markdown'
    });
});

module.exports = router;
