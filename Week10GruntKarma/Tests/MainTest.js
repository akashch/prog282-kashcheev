/**
 * @author Charlie Calvert
 */

require.config({
    baseUrl: "/",
    paths: {
        "jquery": "public/javascripts/jquery-2.1.0.min",
        "DefaultReader": "public/javascripts/Readers/DefaultReader",
        "JsonReader": "public/javascripts/Readers/JsonReader",
        "MarkdownReader": "public/javascripts/Readers/MarkdownReader",
        "ReaderBridge": "public/javascripts/Bridges/ReaderBridge",
        "FancyReaderBridge": "public/javascripts/Bridges/FancyReaderBridge",
        "BridgeFactory": "public/javascripts/Factories/BridgeFactory",
        "ReaderFactory": "public/javascripts/Factories/ReaderFactory",
        "ReaderTests": "ReaderTests",
        'jasmine': 'jasmine-2.0.0/jasmine',
        'jasmine-html': 'jasmine-2.0.0/jasmine-html',
        'boot': 'jasmine-2.0.0/boot',
        'DisplayAddress': "public/javascripts/Display/DisplayAddress",
        'DisplayFileList': 'public/javascripts/Display/DisplayFileList',
        'Utilities': "public/javascripts/Utilities"
    },
    shim: {
        'jasmine': {
            exports: 'jasmine'
        },
        'jasmine-html': {
            deps: ['jasmine'],
            exports: 'jasmine'
        },
        'boot': {
            deps: ['jasmine', 'jasmine-html'],
            exports: 'jasmine'
        }
    }
});

require(['boot'], function(jasmine) {
    'use strict';

    require(["jquery", "ReaderTests", "AsyncTests", "BridgeTests"],
        function(jq, ReaderTests, AsyncTests, BridgeTests) {
            console.log("Main called.");
            $("p").hide();
            window.onload();
        });
});
