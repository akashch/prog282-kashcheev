/**
 * @author Charlie Calvert
 */

define(["ReaderFactory", "JsonReader", "MarkdownReader", "DefaultReader"],
    function(ReaderFactory, JsonReader, MarkdownReader, DefaultReader) {
        'use strict';

        describe("Reader Tests", function() {

            it("proves we can run a test", function() {
                expect(true).toBe(true);
            });

            it("proves we can create a ReaderFactory, JasonReader and MarkdownReader", function() {
                expect(ReaderFactory).toBeTruthy();
                expect(JsonReader).toBeTruthy();
                expect(MarkdownReader).toBeTruthy();
            });

            it("proves we can create a JsonReader with our ReaderFactory", function() {
                var readerFactory = new ReaderFactory();
                var jsonReader = readerFactory.create({
                    objectType: "JsonReader"
                });
                expect(jsonReader).toBeTruthy();
                expect(jsonReader instanceof JsonReader).toBe(true);
            });

            it("proves we can create a MarkdownReader with our ReaderFactory", function() {
                var readerFactory = new ReaderFactory();
                var markdownReader = readerFactory.create({
                    objectType: "MarkdownReader"
                });
                expect(markdownReader).toBeTruthy();
                expect(markdownReader instanceof MarkdownReader).toBe(true);
            });

            it("proves we can create a DefaultReader with our ReaderFactory", function() {
                var readerFactory = new ReaderFactory();
                var defaultReader = readerFactory.create({
                    objectType: "DefaultReader"
                });
                expect(defaultReader).toBeTruthy();
                expect(defaultReader instanceof DefaultReader).toBe(true);
            });

        });
    });
