/**
 * @author Charlie Calvert
 */

require.config({
	paths: {
        "jquery": "jquery-2.1.1",
        "bootstrap": "bootstrap.min"
    },
    shim: {
        // Lots omitted here
        'bootstrap': {
            deps: ['jquery']
        }
    }
});

function endsWith(value, suffix) {
    return value.indexOf(suffix, this.length - suffix.length) !== -1;
}

require([ 'bootstrap', 'jquery'],
function(bootstrap, jquery) {
    'use strict';
    console.log("Main called");


});
