define(function(require) {'use strict';

    var elf = {};
    elf.run = {};

    elf.ClickEvents = ( function() {
            var listItem = $(".listItem");
            var intro = $("#intro");

            function ClickEvents() {
                $(intro).html("ClickEvents is loaded. Click items on this page.");
                $(intro).addClass('blue');
                $(listItem).click(listClick);                
            }

            var listClick = function(event) {
				clear();
				addParagraphs();
				$.getJSON('/' + event.target.innerText, function(data) {
					addText(data);
				});
            };
            
            var addText = function(data){
				$("#result").text(data.result);
				$("#route").text(data.route);
				$("#message").text(data.message);
			};
            
            var addParagraphs = function(){
				$('body').append($('<p>', {id: 'result'}));
				$('body').append($('<p>', {id: 'route'}));
				$('body').append($('<p>', {id: 'message'}));
			};
            
            var clear = function(){
				$('.short').remove();
				$('a').remove();
				$('h2').html("Response from Server");
				$('#result').remove();
				$('#route').remove();
				$('#message').remove();
			};

            return ClickEvents;

        }());

    return elf.ClickEvents;

});
