/**
 * @author Andrey Kashcheev
 */


describe("Week03 Queue and Stack", function() {
	'use strict';
	
	it("checks that Jasmine is working", function() {
		expect(true).toBe(true);
	});
	
	describe("Tests suite for Queue", function() {
		'use strict';
		var myQueue = null;
				
		beforeEach(function(){
			myQueue = new TheQueue();
		});
		
		function loadDefaultValues(){
			myQueue.queue('alpha');
			myQueue.queue('bravo');
			myQueue.queue('charlie');
		}
		
		
		it("proves that contructor does not return null", function() {
			expect(myQueue).not.toBeNull();
		});
		
		it("proves that freshly contructed queue is empty", function() {
			expect(myQueue.Length).toEqual(0);
		});
		
		it("proves front and back returns firts and last elements and proves that it doesnt change lenght", function() {
			loadDefaultValues();
			var len = myQueue.Length;
			expect(myQueue.front()).toEqual('alpha');
			expect(myQueue.back()).toEqual('charlie');
			expect(myQueue.Length).toEqual(len);
		});
		
		it("proves that dequeue removes elements from queue", function() {
			loadDefaultValues();
			expect(myQueue.Length).toEqual(3);
			while(myQueue.Length != 0){
				myQueue.dequeue();
			}
			expect(myQueue.Length).toEqual(0);
		});
		
		it("proves that dequeue first returns alpha, then bravo then charlie", function() {
			loadDefaultValues();
			for(var i = 0; i < myQueue.Length;i++){
				switch(i){
					case 0: expect(myQueue.dequeue()).toEqual('alpha');
						break;
					case 1: expect(myQueue.dequeue()).toEqual('bravo');
						break;
					case 2: expect(myQueue.dequeue()).toEqual('charlie');
						break;
				}
			}
		});
		
		it("proves that 100,000 items have been inserted and that the first item is Item000001 and the last is Item100000." + 
		    "Also. it proves that the first 3 dequeued items are Item000001, Item000002 and Item000003", function() {
			for(var i = 1; i <= 100000;i++){
				myQueue.queuePadNumber(i,6,0, 'Item');
			}
			expect(myQueue.front()).toBe('Item000001');
			expect(myQueue.back()).toBe('Item100000');
			expect(myQueue.dequeue()).toBe('Item000001');
			expect(myQueue.dequeue()).toBe('Item000002');
			expect(myQueue.dequeue()).toBe('Item000003');
		});
	});
	
	describe("Tests suite for Stack", function() {
		'use strict';
		
		var myStack = null;
		
		beforeEach(function(){
			myStack = new TheStack();
		});
		
		function loadDefaultValues(){
			myStack.push('alpha');
			myStack.push('bravo');
			myStack.push('charlie');
		}
	
		it("proves that Stack has been created", function() {
			expect(myStack).not.toBeNull();
		});
		
		it("proves that 3 elements have pushed into Stack", function() {
			loadDefaultValues();
			expect(myStack.Length).toEqual(3);
		});
		
		it("proves that pop returns last pushed element", function() {
			loadDefaultValues();
			expect(myStack.pop()).toBe('charlie');
		});
		
		it("proves that after three elements have been pushed and doing pop twice returns lenght 1", function() {
			loadDefaultValues();
			for(var i = 0; i<2;i++) myStack.pop();
			expect(myStack.Length).toEqual(1);
		});
		
		it("proves that Stack class throw custom exception if pop was executed for when no elements", function() {
			expect(function(){ myStack.pop(); }).toThrow(new TheException("There is no elements in stack"));
		});
		
		it("proves that testPalindrome method works properly", function() {
			expect(myStack.testPalindrome('230032')).toBe(true);
			myStack.clean();
			expect(myStack.testPalindrome('230035')).toBe(false);
			myStack.clean();
			expect(myStack.testPalindrome('Was it a car or a cat I saw?')).toBe(true);
			myStack.clean();
			expect(myStack.testPalindrome('Was it a bat or a rat that I saw?')).not.toBe(true);
		});
		
		it("proves that removeItem method removes item from middle of Stack and leaves other intact", function() {
			loadDefaultValues();
			myStack.push('delta');
			myStack.push('echo');
			myStack.removeItem();
			expect(myStack.pop()).toEqual('echo');
			expect(myStack.pop()).toEqual('delta');
			expect(myStack.pop()).toEqual('bravo');
			expect(myStack.pop()).toEqual('alpha');
		});
		
	});
	
});