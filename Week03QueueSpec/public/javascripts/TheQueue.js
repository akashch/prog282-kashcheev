/**
 * @author Andrey Kashcheev
 */

function TheQueue(){
	'use strict';
	
	var myQueue = [];
	this.Length = myQueue.length;
	
	var padNumber = function(numberToPad, width, padValue) {
		'use strict';
		padValue = padValue || '0';
		numberToPad = numberToPad + '';
		if (numberToPad.length >= width) {
			return numberToPad;
		} else {
			return new Array(width - numberToPad.length + 1).join(padValue) + numberToPad;
		}
	};

	
	TheQueue.prototype.queue = function Queue(element){
		myQueue.push(element);
		this.Length = myQueue.length;
	};
	
	TheQueue.prototype.queuePadNumber = function QueuePadNumber(element, width, padValue, item){
		myQueue.push(item + '' + padNumber(element, width, padValue));
		this.Length = myQueue.length;
	};
	
	TheQueue.prototype.dequeue = function Dequeue(){
		if(this.Length != 0){
			var element = myQueue.shift();
			this.Length = myQueue.length;
			return element;
		} else{
			throw new TheException("There is no elements in queue");
		}
		
		
	};
	
	//returns first element from queue
	TheQueue.prototype.front = function Front(){
		return myQueue[0];	
	};
	
	//returns first element from queue
	TheQueue.prototype.back = function Back(){
		return myQueue[myQueue.length-1];	
	};
}
