/**
 * @author Andrey Kashcheev
 */
function TheStack(){
	'use strict';
	
	var myStack = [];
	this.Length = myStack.length;
	
	
	TheStack.prototype.push = function Push(element){
		myStack.push(element);
		this.Length = myStack.length;
	};
	
	TheStack.prototype.pop = function Pop(){
		if(this.Length != 0){
			var element = myStack.pop();
			this.Length = myStack.length;
			return element;
		} else {
			throw new TheException("There is no elements in stack");
		}
	};
	
	//returns last element 
	TheStack.prototype.last = function Last(){
		return myStack[myStack.length-1];
	};
	
	//returns first element
	TheStack.prototype.firts = function First(){
		return myStack[0];
	};
	
	TheStack.prototype.clean = function Clean(){
		myStack = [];
	};
	
	//Palindrome
	TheStack.prototype.testPalindrome = function TestPalindrome(value){
		value = stripPunctuation(stripWhiteSpace(value)).toLowerCase();
		storeWord(value);
		return isPalindrome(value);
	};
	
	var storeWord = function(str){
		if(str.length >= 1){
			myStack.push(str[0]);
			return storeWord(str.substring(1, str.Lenght));
		}
	};
	
	var isPalindrome = function(){
		if(myStack.length < 2) 
			return true;
		else if(myStack.shift() == myStack.pop()) 
			return isPalindrome();
		return false;
	};

    
    var stripWhiteSpace = function (value) {
        'use strict';
        return String(value)
            .replace(/ /g, '')
            .replace(/\t/g, '')
            .replace(/\r/g, '')
            .replace(/\n/g, '');    
    };
    
    var stripPunctuation = function(value) {
        'use strict';
        return String(value)
            .replace(/\./g, '')
            .replace(/!/g, '')
            .replace(/\?/g, '')
            .replace(/,/g, ''); 
    };
    
    //removes middle item 
	TheStack.prototype.removeItem = function RemoveItem(){
		var start = Math.ceil((myStack.length)/2 - 1);
		console.log(start);
		for(var i = start; i < myStack.length-1; i++){
			myStack[i] = myStack[i+1];
		}
		myStack.pop();
	};
	
}
