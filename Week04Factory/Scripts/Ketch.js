/**
 * New node file
 */


define(function(require) {

    function Ketch(options) {'use strict';

        // some defaults
        this.sailCount = options.sailCount || 200;
        this.state = options.state || "brand new";
        this.color = options.color || "black";
        this.size = options.size || "huge";
        this.keel = options.keel || true;
        this.mizzen = options.mizzen || false;
    }

    return Ketch;

}); 
	
