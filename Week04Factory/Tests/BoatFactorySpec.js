/*globals describe:true, it:true, expect:true, SailBoatFactory: true, Sloop: true */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(["SailBoatFactory", "Sloop", "Yawl", "Ketch"], function(SailBoatFactory, Sloop, Yawl, Ketch) {'use strict';

// define([], function() {'use strict';
    describe("Simple Factory Suite", function() {
        
        //var SailBoatFactory = require('../Factory/SailBoatFactory');
        //var Sloop = require('../Factory/Sloop');
        //var Yawl = require('../Factory/Yawl'); 

        it("proves we can run a test", function() {
            expect(true).toBe(true);
        });

		describe("Test Suite for Sloop", function() {
			
			it("creates a sloop", function() {
	            var boatFactory = new SailBoatFactory();
	            var sloop = boatFactory.createBoat({
	                boatType : "sloop",
	                color : "yellow",
	                sails : 3
	            });
	            expect( sloop instanceof Sloop).toBe(true);
        	});
        	
        	
        	it("tests that sloop was created with correct deafult values", function() {
	            var boatFactory = new SailBoatFactory();
	            var sloop = boatFactory.createBoat({
	                boatType : "sloop"
	            });
	            expect( sloop instanceof Sloop).toBe(true);
				expect( sloop.state).toBe("brand new");
	            expect( sloop.color).toBe("silver");
	            expect( sloop.keel).toBe(true);
	            expect( sloop.mizzen).not.toBe(true);
        	});
        	
        	
        	it("creates a yellow sloop", function() {
	            var boatFactory = new SailBoatFactory();
	            var sloop = boatFactory.createBoat({
	                boatType : "sloop",
	                color : "yellow",
	                sails : 3
	            });
	            expect(sloop.color).toBe('yellow');
        	});
        	
        	
		});
		
		describe("Test Suite for Yawl", function() {
			
			it("creates a yawl", function() {
	            var boatFactory = new SailBoatFactory();
	            var yawl = boatFactory.createBoat({
	                boatType : "yawl",
	                color : "yellow",
	                sails : 3
	            });
	            expect( yawl instanceof Yawl).toBe(true);
        	});
        	
			it("tests that yawl was created with correct deafult values", function() {
	            var boatFactory = new SailBoatFactory();
	            var yawl = boatFactory.createBoat({
	                boatType : "yawl"
	            });
	            expect( yawl instanceof Yawl).toBe(true);
				expect( yawl.state).toBe("used");
	            expect( yawl.color).toBe("blue");
	            expect( yawl.wheelSize).toBe("large");
	            expect( yawl.mizzen).toBe(true);
        	});
        	
			
			
	        it("shows that a yawl has a mizzen", function() {
	            var boatFactory = new SailBoatFactory();
	            var yawl = boatFactory.createBoat({
	                boatType : "yawl",
	                color : "yellow",
	                sails : 3
	            });
	            expect(yawl.mizzen).toBe(true);
	        });
       });
       
       describe("Test Suite for Ketch", function() {
       		it("creates a ketch", function() {
	            var boatFactory = new SailBoatFactory();
	            var ketch = boatFactory.createBoat({
	                boatType : "ketch"
	            });
	            expect(ketch instanceof Ketch).toBe(true);
        	});
        	
        	it("tests that sloop was created with correct deafult values", function() {
	            var boatFactory = new SailBoatFactory();
	            var ketch = boatFactory.createBoat({
	                boatType : "ketch"
	            });
	            expect( ketch instanceof Ketch).toBe(true);
				expect( ketch.state).toBe("brand new");
	            expect( ketch.color).toBe("black");
	            expect( ketch.size).toBe("huge");
	            expect( ketch.keel).toBe(true);
	            expect( ketch.mizzen).not.toBe(true);
        	});
       });
    });

}); 
