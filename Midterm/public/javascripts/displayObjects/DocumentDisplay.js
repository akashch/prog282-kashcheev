/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
	var define = require('amdefine')(module);
}

define(function(require) {'use strict';

	var DocumentDisplay = (function() {

		var tempJson = null;
		var myBridges = null;
		var flag = false;
		function DocumentDisplay(options) {
			this.type = "DocumentDisplay";
		}

		DocumentDisplay.prototype.display = function(myJsonBridgeReader, route) {
			clear();
			var tempJson = {
				"method" : "read",
				"path" : route};
			myJsonBridgeReader.read(tempJson, function(data) {
				show(JSON.parse(data.text));
			});
		};

		DocumentDisplay.prototype.storeBridges = function(theBridges) {
			myBridges = theBridges;
		};

		var show = function(data) {
			var i = null;
			tempJson = data.content;
			var lenght = data.content.length;
			$('#title').html(data.title.charAt(0).toUpperCase() + data.title.slice(1));
			for ( i = 0; i < lenght; i++) {
				$("#content").append("<li>" + data.content[i].type  +  (data.content[i].keywords !== undefined ? " - " + data.content[i].keywords : "")  + "</li>");
			}
			$("ul li").addClass("listItem");
			$(".listItem").click(listClick);
			data.content[0].keywords !== undefined ? flag = true : "";
			
		};

		var clear = function() {
			$("#content").empty();
		};

		var listClick = function() {
			console.log(myBridges);
			console.log(flag);
			if(!flag) myBridges.myDisplayDocumentBridge.display(myBridges.myJsonBridgeReader, search(event.target.innerText).path);
			else myBridges.myDisplayDefaultBridge.display(myBridges.myJsonBridgeReader);
			flag = false;
		};

		var search = function(data){
			var length = tempJson.length;
			for(var i = 0; i < length; i++){
				if(tempJson[i].type === data){
					return tempJson[i];
				}
			}
		};

		return DocumentDisplay;
	})();

	return DocumentDisplay;

}); 