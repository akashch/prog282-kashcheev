/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
	var define = require('amdefine')(module);
}

define(function(require) {'use strict';

	var DefaultDisplay = (function() {

		var myBridges = null;
		var tempJson = null;
		DefaultDisplay.prototype.settingsJson = {
			"method" : "read",
			"path" : "metafiles/MainSettings.json"
		};
		var that = null;

		function DefaultDisplay(options) {
			this.type = "DefaultDisplay";
			that = this;
		}


		DefaultDisplay.prototype.display = function(myJsonBridgeReader, route) {
			clear();
			console.log("route --> " + this.settingsJson);
			myJsonBridgeReader.read(this.settingsJson, function(data) {
				show(JSON.parse(data.text));
			});
		};

		DefaultDisplay.prototype.storeBridges = function(theBridges) {
			myBridges = theBridges;
		};

		var show = function(data) {
			var i = null;
			tempJson = data.content;
			var lenght = data.content.length;
			$('#title').html(data.title.charAt(0).toUpperCase() + data.title.slice(1));
			for ( i = 0; i < lenght; i++) {
				$("#content").append("<li>" + data.content[i].type + "</li>");
			}
			$("ul li").addClass("listItem");
			$(".listItem").click(listClick);
		};

		var clear = function() {
			$("#content").empty();
		};

		var listClick = function() {
			myBridges.myDisplayDocumentBridge.storeBridges(myBridges);
			myBridges.myDisplayDocumentBridge.display(myBridges.myJsonBridgeReader, search(event.target.innerText).path);
		};
		
		var search = function(data){
			var length = tempJson.length;
			for(var i = 0; i < length; i++){
				if(tempJson[i].type === data){
					return tempJson[i];
				}
			}
		};

		return DefaultDisplay;
	})();

	return DefaultDisplay;

}); 