/**
 * @author Andrey Kashcheev
 */
/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
	var define = require('amdefine')(module);
}

define(function(require) {'use strict';

	var DefaultReader = function(options) {
		this.path = options.path || null;
		this.type = "DefaultReader";
	};

	DefaultReader.prototype.read = function(path, callback) {
		this.path = path.path;
		$.getJSON("/users/" + this.path, function(data) {
			callback(data);
		});
	};

	DefaultReader.prototype.write = function() {
		return "DefaultReader write method needs to be implemented";
	};

	DefaultReader.prototype.remove = function() {
		return "DefaultReader remove method needs to be implemented";
	};

	return DefaultReader;

}); 