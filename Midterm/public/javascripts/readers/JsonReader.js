/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {'use strict';

	var jQuery = require('jquery');
	
    var JsonReader = function(options) {
		this.path = options.path;
		this.type = "JsonReader";
	};

    JsonReader.prototype.read = function(settingsJson, callback){
		this.path = settingsJson.method || "read";
		console.log(this.path);
		$.getJSON( "/users/" + this.path, settingsJson, function(data) {
			callback(data);
		});
    };

	JsonReader.prototype.write = function(){
		return "JSON has been successfully written";
	};

    JsonReader.prototype.remove = function(){
		return "JSON has been successfully deleted";
	};

    return JsonReader;

}); 