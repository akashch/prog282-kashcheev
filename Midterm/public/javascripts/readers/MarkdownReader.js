/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {'use strict';

    var MarkdownReader = function(options) {
        this.path = options.path || null;
		this.type = "MarkdownReader";
	};

	MarkdownReader.prototype.read = function(path, callback){
		this.path = path.path;
		$.getJSON( "/users/" + this.path, function(data) {
			callback(data);
		});
	};

	MarkdownReader.prototype.write = function(){
		return "Markdown has been successfully written";
	};

	MarkdownReader.prototype.remove = function(){
		return "Markdown has been successfully deleted";
	};

	return MarkdownReader;

});