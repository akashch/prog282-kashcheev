/**
 * @author Andrey Kashcheev
 */

require.config({
	paths : {
		"jquery" : 'misc/jquery-2.1.0.min',
		"jsonReader" : 'readers/JsonReader',
		"markdownReader" : 'readers/MarkdownReader',
		"DefaultReader" : "readers/DefaultReader",
		// "BridgeFactory" : 'factories/BridgeFactory',
		//"ReaderFactory" : "factories/ReaderFactory",
		"TheFactory" : "factories/TheFactory",
		"ReaderBridge" : "bridges/ReaderBridge",
		"FancyReaderBridge" : "bridges/FancyReaderBridge",
		"DisplayBridge" : "bridges/DisplayBridge",
		"DefaultDisplay" : "displayObjects/DefaultDisplay",
		"DocumentDisplay" : "displayObjects/DocumentDisplay",
		"ImageDisplay" : "displayObjects/ImageDisplay",
		"MarkdownDisplay" : "displayObjects/MarkdownDisplay",
		"QuizDisplay" : "displayObjects/QuizDisplay"
	}
});

require(["TheFactory", "DefaultDisplay"], function(TheFactory, DefaultDisplay) {'use strict';
	console.log("Midterm - Main was called");

	var theFactory = new TheFactory();
	var myBridges = {
		myJsonBridgeReader : theFactory.createBridge({
			"type" : "Reader",
			"objType" : theFactory.createReader({
				path : null, // to do
				type : "JsonReader"
			})
		}),
		myDisplayDefaultBridge : theFactory.createBridge({
			"type" : "Display",
			"objType" : theFactory.createDiplayObject({
				type : "Default"
			})
		}),
		myDisplayDocumentBridge : theFactory.createBridge({
			"type" : "Display",
			"objType" : theFactory.createDiplayObject({
				type : "Document"
			})
		})
	};
	myBridges.myDisplayDefaultBridge.storeBridges(myBridges);
	myBridges.myDisplayDefaultBridge.display(myBridges.myJsonBridgeReader);

});
