/**
 * @author Andrey Kashcheev
 */

if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(["ReaderBridge"], function(Reader) {'use strict';
	
	var FancyReader = (function() {
	
		function FancyReaderBridge(reader){
			this.setReader(reader);
		}
		
		FancyReader.prototype = new Reader();
		
		FancyReader.prototype.length = function(path, callback){
			this.reader.read(path, function(data){
				callback(data.text.lenght);
			});
		};
		
        return FancyReaderBridge;
    }());
    
    

    return FancyReaderBridge;
}); 