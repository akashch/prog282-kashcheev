/**
 * @author Andrey Kashcheev
 */

define(function(require) {'use strict';

	var ReaderBridge = (function() {


		function ReaderBridge(reader){
			this.setReader(reader);
		}
				
		ReaderBridge.prototype.setReader = function(reader){
			this.reader = reader;
		};

		ReaderBridge.prototype.read = function(settingsJson, callback){
			this.reader.read(settingsJson, function(data){
				callback(data);
			});
		};
		
		ReaderBridge.prototype.write = function(){
			return this.reader.write();
		};

		ReaderBridge.prototype.remove = function(){
			return this.reader.remove();	
		};
		
		ReaderBridge.prototype.getPath = function(){
			return this.reader.path;
		};
		
		ReaderBridge.prototype.setPath = function(path){
			this.reader.path = path;
		};
		
		return ReaderBridge;
	}());

	return ReaderBridge;
});