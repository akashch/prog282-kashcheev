/**
 * @author Andrey Kashcheev
 */

define(function(require) {'use strict';

	var DisplayBridge = (function() {

		function DisplayBridge(displayObj){
			this.setDisplay(displayObj);
		}
		
		DisplayBridge.prototype.storeBridges = function(theBridges){
			this.displayObj.storeBridges(theBridges);
		};
				
		DisplayBridge.prototype.setDisplay = function(displayObj){
			this.displayObj = displayObj;
		};

		DisplayBridge.prototype.display = function(myJsonBridgeReader, route){
			this.displayObj.display(myJsonBridgeReader, route);
		};
		
		
		return DisplayBridge;
	}());

	return DisplayBridge;
});