/**
 * @author Andrey Kashcheev
 */
if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {
	'use strict';
	
	//Readers
	var jsonReader = require("jsonReader");
	var markdownReader = require("markdownReader");
	var defaultReader = require("DefaultReader");
	
	//Bridges
	var readerBridge = require("ReaderBridge");
	//var fancyReaderBridge = require("FancyReaderBridge");
	var displayBridge = require('DisplayBridge');
	
	//diplays
	var defaultDisplay = require("DefaultDisplay");
	var documentDisplay = require("DocumentDisplay");
	var imageDisplay = require("ImageDisplay");
	var markdownDisplay = require("MarkdownDisplay");
	var quizDisplay = require("QuizDisplay");
	
	
	


	function TheFactory(){}
	
	TheFactory.prototype.readerType = null;
	TheFactory.prototype.bridgeType = null;
	TheFactory.prototype.displayType = null;
	
	TheFactory.prototype.createReader = function(options){
		switch(options.type){
			case 'JsonReader': this.readerType = jsonReader;
				break;
			case 'MarkdownReader': this.readerType = markdownReader;
				break;
			default: this.readerType = jsonReader;
		}
		
		return new this.readerType(options);
	};
	
	
	TheFactory.prototype.createBridge = function(options){
		switch(options.type){
			case "Reader" : this.bridgeType = readerBridge;
				break;
			case "FancyReader" : this.bridgeType = fancyReaderBridge;
				break;
			case "Display" : this.bridgeType = displayBridge;
				break;
			default: this.bridgeType = readerBridge;
		}
		
		return new this.bridgeType(options.objType);
	};
	
	TheFactory.prototype.createDiplayObject = function(options){
		switch(options.type){
			case "Default" : this.displayType = defaultDisplay;
				break;
			case "Document" : this.displayType = documentDisplay;
				break;
			case "Image" : this.displayType = imageDisplay;
				break;
			case "Markdown" : this.displayType = markdownDisplay;
				break;
			case "Quiz" : this.displayType = quizDisplay;
				break;
			default: this.displayType = defaultDisplay;
		}
		return new this.displayType();
	};

    return TheFactory;
});