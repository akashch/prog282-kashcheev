/**
 * @author Andrey Kashcheev
 */
if ( typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define(function(require) {
	'use strict';
	//Readers
	var jsonReader = require("JsonReader");
	var markdownReader = require("MarkdownReader");
	var defaultReader = require("DefaultReader");


	function TheFactory(){}
	
	TheFactory.prototype.readerType = null;
	
	TheFactory.prototype.createReader = function(options){
		switch(options.type){
			case 'JsonReader': this.readerType = jsonReader;
				break;
			case 'MarkdownReader': this.readerType = markdownReader;
				break;
			default: this.readerType = defaultReader;
		}
		
		return new this.readerType(options);
	};

    return TheFactory;
});