/**
 * @author Andrey Kashcheev
 */

require.config({
    paths : {
        "JsonReader" : '/javascripts/readers/JsonReader',
        "MarkdownReader" : '/javascripts/readers/MarkdownReader',
        "DefaultReader" : "/javascripts/readers/DefaultReader",
        "BridgeFactory" : '/javascripts/factories/BridgeFactory',
        "ReaderFactory" : "/javascripts/factories/ReaderFactory",
        "TheFactory" : "/javascripts/factories/TheFactory",
        "ReaderBridge" : "/javascripts/bridges/ReaderBridge",
        "FancyReaderBridge" : "/javascripts/bridges/FancyReaderBridge",
        "DisplayBridge" : "/javascripts/bridges/DisplayBridge",
        "DefaultDisplay" : "/javascripts/displayObjects/DefaultDisplay",
        "DocumentDisplay" : "/javascripts/displayObjects/DocumentDisplay",
        "ImageDisplay" : "/javascripts/displayObjects/ImageDisplay",
        "MarkdownDisplay" : "/javascripts/displayObjects/MarkdownDisplay",
        "QuizDisplay" : "/javascripts/displayObjects/QuizDisplay",
        'JQuery' : '/javascripts/misc/jquery-2.1.0.min',
        'jasmine' : 'jasmine-2.0.0/jasmine',
        'jasmine-html' : 'jasmine-2.0.0/jasmine-html',
        'boot' : 'jasmine-2.0.0/boot'
    },
    shim : {
        'jasmine' : {
            exports : 'jasmine'
        },
        'jasmine-html' : {
            deps : ['jasmine'],
            exports : 'jasmine'
        },
        'boot' : {
            deps : ['jasmine', 'jasmine-html'],
            exports : 'jasmine'
        }
    }
});

/*
 * Do this two step dance with two requires 
 * when you want to load jasmine.
 */ 
require(['boot'], function(boot) {
    'use strict';
    
    // Load the specs with second call to require
    require(["MidTermSpec"], function() {
        console.log("Main called.");    
        window.onload();
    });
});

