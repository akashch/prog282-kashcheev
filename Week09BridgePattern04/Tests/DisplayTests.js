/**
 * @author Andrey Kashcheev
 */

define(["DisplayFactory", "BridgeFactory", "ReaderFactory", "DisplayAddress", "DisplayFileList"], 
    function(DisplayFactory, BridgeFactory, ReaderFactory, DisplayAddress, DisplayFileList) {
    'use strict';

    describe("Display Tests", function() {

        it("proves we can run a test", function() {
            expect(true).toBe(true);
        });
        
        it("proves we can create a DisplayFactory", function() {
            var displayFactory = new DisplayFactory();
            expect(displayFactory).toBeTruthy();
        });
        
		it("proves we can display method for DisplayAddress", function() {
            var spy = spyOn(DisplayAddress.prototype, "display");
            var displayAddress = new DisplayAddress();
            displayAddress.display();
            expect(spy).toHaveBeenCalled();
        });
        
       it("proves we can display method for DisplayFileList", function() {
            var spy = spyOn(DisplayFileList.prototype, "display");
            var displayFileList = new DisplayFileList();
            displayFileList.display();
            expect(spy).toHaveBeenCalled();
        });
		
    });
});