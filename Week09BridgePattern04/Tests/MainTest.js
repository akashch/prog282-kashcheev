/**
 * @author Andrey Kashcheev
 */

require.config({
    baseUrl : "/",
    paths : {
        "jquery" : "javascripts/jquery-2.1.1",
        "Control" : "javascripts/Control",
        "PubSub" : "javascripts/TinyPubSub",
        "DisplayFactory": "javascripts/Factories/DisplayFactory",
        "DisplayFactory": "javascripts/Factories/DisplayFactory",
        "DisplayAddress": "javascripts/Display/DisplayAddress",
        "DisplayFileList": "javascripts/Display/DisplayFileList",
        "ReaderFactory": "javascripts/Factories/ReaderFactory",
        "BridgeFactory": "javascripts/Factories/BridgeFactory",
        "JsonReader": "javascripts/Readers/JsonReader",
        "DefaultReader": "javascripts/Readers/DefaultReader",
        "MarkdownReader": "javascripts/Readers/MarkdownReader",
        "FancyReaderBridge": "javascripts/Bridges/FancyReaderBridge",
        "ReaderBridge": "javascripts/Bridges/ReaderBridge",
        "Utilities": "javascripts/Utilities",
        'jasmine' : 'jasmine/jasmine',
        'jasmine-html' : '/jasmine/jasmine-html',
        'boot' : 'jasmine/boot'
    },
    shim : {
        'jasmine' : {
            exports : 'jasmine'
        },
        'jasmine-html' : {
            deps : [ 'jasmine' ],
            exports : 'jasmine'
        },
        'boot' : {
            deps : [ 'jasmine', 'jasmine-html' ],
            exports : 'jasmine'
        }
    }
});

require([ 'boot' ], function(jasmine) {
    'use strict';

    require([ "jquery", "BridgeTests", "DisplayTests", "DefaultSingletonTests", "ReaderTests", "ControlTests"], 
        function(jq, BridgeTests, DisplayTests, DefaultSingletonTests, ReaderTests, controlTests) {
        console.log("Main called.");
        $("p").hide();
        window.onload();
    });
});