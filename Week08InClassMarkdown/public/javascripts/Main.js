/**
 * @author Andrey Kashcheev
 */
require.config({
    paths : {
        "jquery" : "jquery-2.1.1",
        "Markdown" : "Markdown/Converter",
        "Editor" : "Markdown/Editor"
    }
});

require(['jquery', 'MarkShow'], function(jq, MarkShow){
	console.log("Main called");
	var markShow = new MarkShow();
});
