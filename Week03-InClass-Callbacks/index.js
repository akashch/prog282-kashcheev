/**
 * @author Charlie
 */

function hello(func) {
    'use strict';
    $("#test02").html("It works! ");
    func();
}

function returnValue(callback){
	return callback(3.14);
}

$(document).ready(function() {
    "use strict";

    $("#test01").html("Document Ready called");
	
	var callback = function(value){
		return value;
	};
		
	var value = returnValue(callback);
	$('#test04').html(value);


    hello(function() {
        $("#test03").html("It's a nine!");
    });
});
