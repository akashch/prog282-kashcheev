/**
 * @author Andrey Kashcheev
 */

define(['index', 'indexUi'], function(sub, pub) {'use strict';
	describe("PROG 282 - Week02-Callbacks", function() {
		
		beforeEach(function() {
				
		}); 
		
		describe("General tests", function() {
			it("Proves Jasmine is working", function() {
				expect(true).toBe(true);
			});
			it("Proves subscriber is accessible", function() {
				expect(sub).toBeTruthy();
			});
			
			it("Proves publisher is accessible", function() {
				expect(pub).toBeTruthy();
			});
		});
		
		describe("Testing methods for Pub Sub Pattern", function() {
			it("Performs Async test on add method", function(done) {
				var event = {
						val01 : 2,
						val02 : 1,
						acknowledge : function(value) {
							expect(value).toEqual(3);
							done();
						}
				};				
				$.publish('add', event);
			});
			
			it("Performs Async test on remaind method", function(done) {
				var event = {
						val01 : 12,
						val02 : 10,
						acknowledge : function(value) {
							expect(value).toEqual(2);
							done();
						}
				};				
				$.publish('remaind', event);
			});
			
			it("Performs Async test on milesToFeet method", function(done) {
				var event = {
						val01 : 2,
						val02 : 1,
						acknowledge : function(value) {
							expect(value).toEqual(5280);
							done();
						}
				};				
				$.publish('milesToFeet', event);
			});	
		});
	});
});
