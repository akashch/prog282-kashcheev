/**
 * @author Andrey Kashcheev
 */

// Publisher
define(['jquery', 'tinyPubSub'], function() {
	
	function calculatorUi(){
		console.log("Publisher constructor callled");//debug
		$("#add").click(addMethod);
		$("#remaind").click(remaindMethod);	
		$("#milesToFeet").click(milesToFeet);
	}
	
	function addMethod(){
		console.log("Inside addMethod");//debug
		var event = {
				message: 'Publisher addMethod called by calculator',
				val01 : $('#val01').val(),
				val02 : $('#val02').val(),
				acknowledge : function(value) {
					$("#result").append("<li>Sum = " + value +  "</li>");
				}
		};
		$.publish('add', event);
	}
	
	function remaindMethod(){
		console.log("Inside remaindMethod");//debug
		var event = {
				message: 'Publisher remaindMethod called by calculator',
				val01 : $('#val01').val(),
				val02 : $('#val02').val(),
				acknowledge : function(value) {
					$("#result").append("<li> Remainder = " + value +  "</li>");
				}
		};
		$.publish('remaind', event);
	}
	
	function milesToFeet(){
		console.log("Inside milesToFeet");//debug
		var event = {
				message: 'Publisher milesToFeet called by calculator',
				val01 : $('#val01').val(),
				val02 : $('#val02').val(),
				acknowledge : function(value) {
					$("#result").append("<li>" + value +  " feet</li>");
				}
		};
		$.publish('milesToFeet', event);
	}

	return {calculatorUi: calculatorUi};
});
