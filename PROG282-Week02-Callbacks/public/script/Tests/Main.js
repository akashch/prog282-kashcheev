/**
 * @author Andrey Kashcheev
 */

require.config({
  paths: {
    "jquery": "http://code.jquery.com/jquery-1.11.0.min",
    "tinyPubSub": "TinyPubSub",
    'jasmine' : 'Library/jasmine',
    'jasmine-html' : 'Library/jasmine-html',
    'boot' : 'Library/boot',
    "unitTest" : "Test01"    
  },
	shim : {
		'jasmine' : {
			exports : 'jasmine'
		},
		'jasmine-html' : {
			deps : ['jasmine'],
			exports : 'jasmine'
		},
		'boot' : {
			deps : ['jasmine', 'jasmine-html'],
			exports : 'jasmine'
		}
	}
});

require(["boot"], function(boot) {
	require(["index", "indexUi","Test01"], function(sub, pub) {
		console.log("Main Test called.");
		sub.calculator();
		pub.calculatorUi();
		// Initialize the HTML Reporter and execute the environment (setup by `boot.js`)
		window.onload();

	});

});
