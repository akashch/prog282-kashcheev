/**
 * @author Andrey Kashcheev
 */

require.config({
  paths: {
    "jquery": "http://code.jquery.com/jquery-1.11.0.min",
    "tinyPubSub": "TinyPubSub"
	}
});

require(["index", "indexUi"], function(sub, pub) {
	console.log("Main called");
	sub.calculator();
	pub.calculatorUi();
});
