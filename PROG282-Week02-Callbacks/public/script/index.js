/**
 * @author Andrey Kashcheev
 */

//Subscriber
define(['jquery', 'tinyPubSub'], function() {
	
	function calculator(){
		console.log("Subscriber constructor called"); //debug
		$.subscribe('add', add);
		$.subscribe('remaind', remaind);	
		$.subscribe('milesToFeet', milesToFeet);
	}
	
	function add(objectSender, eventArgs){
		eventArgs.acknowledge(callbackHandler(addition, eventArgs.val01, eventArgs.val02));
	}
	
	function remaind(objectSender, eventArgs){
		eventArgs.acknowledge(callbackHandler(remainder, eventArgs.val01, eventArgs.val02));
	}
	
	function milesToFeet(objectSender, eventArgs){
		eventArgs.acknowledge(callbackHandler(converter, eventArgs.val01, eventArgs.val02));
	}
	
	//Miles to Feet
	function converter(val01, val02){
		return 5280*parseInt(val02);
	}
	
	//Remainder
	function remainder(val01, val02){
		return parseInt(val01) % parseInt(val02);
	}
	
	//Addition
	function addition(val01, val02){
		return parseInt(val01) + parseInt(val02);
	}
	
	function callbackHandler(func, val01, val02){
		return func(val01, val02);
	}
	
	return {calculator: calculator};
	

});
