var express = require('express');
var app = express();
var format = require('util').format;
var fs = require('fs');


// Default.
app.get('/', function(request, result){
  	var html = fs.readFileSync(__dirname + '/public/index.html');
	result.writeHeader(200, {"Content-Type": "text/html"});   
	result.write(html);
	result.end();
});

// Give express access to the Public directory
app.use("/", express.static(__dirname + '/public'));

// Tell the webserver (and user) to listen on port 30025
app.listen(30025);
console.log('Listening on port 30025');
