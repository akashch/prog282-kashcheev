var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.send({ "result": 'Hi from address' });
});

router.get('/read', function(request, response) {
	var queryObject = request.query;
	var queryAsString = JSON.stringify(request.query);
	console.log("Read called: " + queryAsString);
	response.send({ "FullName": queryObject.firstName +  " "+ queryObject.lastName});
});

module.exports = router;
