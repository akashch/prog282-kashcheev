var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.render('index', { title: 'Week07InClassRoute' });
});

router.get('/read', function(request, response) {
    response.send({ "result": "The server reports success from routes/index.js" });
});

module.exports = router;
