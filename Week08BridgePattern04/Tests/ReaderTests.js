/**
 * @author Andrey Kashcheev
 */

define(["Control", "JsonReader", "MarkdownReader", "DefaultReader", "ReaderFactory"], 
	function(Control, JsonReader, MarkdownReader, DefaultReader, ReaderFactory) {'use strict';

	describe("Reader Tests", function() {

	var a, b, c, d, f;
	
		beforeEach(function() {
			a = new JsonReader();
			b = new JsonReader();
			c = new JsonReader();
			d = new JsonReader();
			f = [];
		});

		it("proves we can run a test", function() {
			expect(true).toBe(true);
		});

		it("proves we can run a sanity check that we expect to fail", function() {
			var e = {};
			expect(a === e).toBe(false);
		});

		it("proves we can create a JsonReader01", function() {
			expect(a === b).toBe(true);
		});

		it("proves we can create a JsonReader02", function() {
			expect(a === c).toBe(true);
		});

		it("proves we can run another sanity check", function() {
			expect(a === f).toBe(false);
		});




		it("proves we can create Control", function() {
			var control = new Control();
			expect(control).toBeTruthy();
		});

		it("proves we can create a JsonReader", function() {
			var jsonReader = new JsonReader();
			expect(jsonReader).toBeTruthy();
		});
		
		it("proves we can create a MarkdownReader", function() {
			var markdownReader = new MarkdownReader();
			expect(markdownReader).toBeTruthy();
		});
		
		it("proves we can create a DefaultReader", function() {
			var defaultReader = new DefaultReader();
			expect(defaultReader).toBeTruthy();
		});
		
		it("proves we can create a ReaderFactory", function() {
			var readerFactory = new ReaderFactory();
			expect(readerFactory).toBeTruthy();
		});
				
		it("proves we can create a DefaultReader", function() {
			var defaultReader = new DefaultReader();
			expect(defaultReader).toBeTruthy();
		});

		it("proves we can create a JsonReader and read something", function(done) {
			var jsonReader = new JsonReader();
			jsonReader.readFile("FileList.json", function(dataFromServer) {
				expect(dataFromServer.content["President01.json"]).toBe("/data/Presidents01.json");
				done();
			});
		});
		
		it("proves we can create a JsonReader and read  Presidents01.json", function(done) {
			var jsonReader = new JsonReader();
			jsonReader.readFile("../data/Presidents01.json", function(dataFromServer) {
				expect(dataFromServer.content["firstName"]).toBe("George");
				done();
			});
		});		

	});
});
