var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(request, response) {
    response.render('CalculatePage', {
        title : 'Routing CalculatePage'
    });
});


router.get('/getNine', function(request, response) {
	response.send({"number":9});
});

router.get('/add', function(request, response) {
	response.send({"number":(parseInt(request.query.val1) + parseInt(request.query.val2))});
});

router.get('/multiply', function(request, response) {
	response.send({"number":(parseInt(request.query.val1) * parseInt(request.query.val2))});
});




module.exports = router;