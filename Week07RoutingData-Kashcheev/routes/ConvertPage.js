/**
 * @author Andrey Kashcheev
 */

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(request, response) {
    response.render('ConvertPage', {
        title : 'Routing ConvertPage'
    });
});

router.get('/toMiles', function(request, response) {
	var number = parseFloat(request.query.number)* 1.6;	
	response.send({ "number":number});
});

router.get('/toSeconds', function(request, response) {
	var number = parseFloat(request.query.number)*3600;	
	response.send({ "number":number});
});


router.get('/toCelcius', function(request, response) {
	var number = (parseFloat(request.query.number)- 32)/1.8;
	response.send({ "number":number});
});



module.exports = router;