/**
 * @author Andrey Kashcheev
 */


require.config({
    paths : {
        "jquery" : "jquery-2.1.1",
        "Hello": "Tools/Hello",
        "Calculator" : "Tools/Calculator",
        "Converter" : "Tools/Converter",
        "DirName": "Tools/DirName",
        "Utilities": "Tools/Utilities"
    }
});

require([ 'jquery', 'Factory' ], function(jq, Factory) {
    'use strict';   
    console.log("Main called");
    
   	var factory = new Factory();
    factory.create({ 'productType': 'Control'});
});