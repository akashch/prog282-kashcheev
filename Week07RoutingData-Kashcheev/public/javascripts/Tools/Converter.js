/**
 * @author Andrey Kashcheev
 */

define([ 'jquery' ], function(jq) {

    var Converter = (function() {

        function Converter() {
        }
        
        Converter.prototype.toMiles = function() {
			var objToSend = {"number" : $("#convert").val()};
            $.getJSON('/convertPage/toMiles',objToSend, function(data) {
               	$("#convert").val(Math.round(data.number * 100) / 100);
            }).error = errorHandler; 
        };
        
        Converter.prototype.toSeconds = function() {
			var objToSend = {"number" : $("#convert").val()};
            $.getJSON('/convertPage/toSeconds',objToSend, function(data) {
               	$("#convert").val(Math.round(data.number * 100) / 100);
            }).error = errorHandler; 
        };
        
        Converter.prototype.toCelcius = function() {
			var objToSend = {"number" : $("#convert").val()};
            $.getJSON('/convertPage/toCelcius',objToSend, function(data) {
               	$("#convert").val(Math.round(data.number * 100) / 100);
            }).error = errorHandler; 
        };

        return Converter;
    }());
    
    return Converter;
});
