/**
 * @author Andrey Kashcheev
 */

define([ 'jquery' ], function(jq) {

    var Calculator = (function() {

        function Calculator() {
        }
        
        Calculator.prototype.getNine = function() {
            $.getJSON('/calculatePage/getNine', function(data) {
               	$("#result").val(data.number);
            }).error = errorHandler; 
        };
        
        Calculator.prototype.add = function(){
        	var objectToSend = {
        		val1 : $("#operanda").val(),
        		val2 : $("#operandb").val()
        	};
        	
        	$.getJSON('/calculatePage/add', objectToSend, function(data) {
               	$("#result").val(data.number);
            }).error = errorHandler; 
        };
        
        
       Calculator.prototype.multiply = function(){
        	var objectToSend = {
        		val1 : $("#operanda").val(),
        		val2 : $("#operandb").val()
        	};
        	
        	$.getJSON('/calculatePage/multiply', objectToSend, function(data) {
               	$("#result").val(data.number);
            }).error = errorHandler; 
        };
        return Calculator;
    }());
    
    return Calculator;
});
