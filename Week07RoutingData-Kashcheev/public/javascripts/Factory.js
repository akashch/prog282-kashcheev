/**
 * @author Andrey Kashcheev
 */

define([ 'control', "DirName", "Hello", "Calculator" , "Converter" ], function(Control, DirName, Hello, Calculator, Converter) {
    'use strict';

    var Factory = (function() {

        function Factory() {
        }

        // Our factories product is an empty object by default
        Factory.prototype.product = { 'error': 'The factory created nothing' };

        // Create a products
        Factory.prototype.create = function(options) {

            switch (options.productType) {
            case "Control":
                this.product = new Control(this);
                break;
            case "Hello":
                this.product = new Hello();
                break;
            case "DirName": 
                this.product = new DirName();
                break;
           case "Calculator": 
                this.product = new Calculator();
                break;
           case "Converter": 
                this.product = new Converter();
                break;
            default:
                console.log("Returning default object.");
            }

            return this.product;

        };

        return Factory;
    }());

    return Factory
});