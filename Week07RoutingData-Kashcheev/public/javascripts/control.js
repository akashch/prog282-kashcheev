/**
 * @author Andrey Kashcheev
 */

define([ 'jquery', "Factory"], function(jq) {

    var Control = (function() {

        var factory;
      	var calculator = null;
      	var converter = null;
      	
        function Control(initFactory) {
        	factory = initFactory;
        	console.log(factory);
            $("#buttonSayHello").click(getHello);           
            $("#buttomDirName").click(dirName);
            $("#getNine").click(getNine);
            $("#add").click(add);
            $("#multiply").click(multiply);
            $("#toMiles").click(toMiles);
            $("#toSeconds").click(toSeconds);
            $("#toCelcius").click(toCelcius);
            
            
        }
        
        function toMiles(){
        	if(calculator === null){
        		converter = factory.create({productType: 'Converter'});
        	}else
        	{
        		converter = factory.create({productType: 'Converter'});
        	}
        	converter.toMiles();	
        }
        
        function toSeconds(){
        	if(calculator === null){
        		converter = factory.create({productType: 'Converter'});
        	}else
        	{
        		converter = factory.create({productType: 'Converter'});
        	}
        	converter.toSeconds();	
        }
        
        function toCelcius(){
        	if(calculator === null){
        		converter = factory.create({productType: 'Converter'});
        	}else
        	{
        		converter = factory.create({productType: 'Converter'});
        	}
        	converter.toCelcius();	
        }
        
        
        
        function add(){
        	if(calculator === null){
        		calculator = factory.create({productType: 'Calculator'});
        	}else
        	{
        		calculator = factory.create({productType: 'Calculator'});
        	}
        	calculator.add();
        }
        
        function multiply(){
        	if(calculator === null){
        		calculator = factory.create({productType: 'Calculator'});
        	}else
        	{
        		calculator = factory.create({productType: 'Calculator'});
        	}
        	calculator.multiply();
        }
        
        function getNine(){
        	if(calculator === null){
        		calculator = factory.create({productType: 'Calculator'});
        	}else
        	{
        		calculator = factory.create({productType: 'Calculator'});
        	}
        	calculator.getNine();
        	
        }

        function dirName() {
            var dirName = factory.create({productType: 'DirName'});
            dirName.getDirName();
        }
        
        function getHello() {
            var hello = factory.create({productType: 'Hello'});
            hello.getHello();
        }

        return Control;

    }());

    return Control;
});