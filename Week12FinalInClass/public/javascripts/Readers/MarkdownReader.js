/**
 * @author Charlie Calvert
 */

define(function(require) {'use strict';

	var MarkdownReader = ( function() {

			function MarkdownReader() {

			}


			MarkdownReader.prototype.readFile = function(fileName, customCallback) {
				$.getJSON('/setPick', {
					pick : fileName
				}, function(result) {
					if (result.result !== "success") {
						throw "Error";
					}
					window.location.href = '/Markdown';
				}).error = function(f, a, b) {
					alert(f);
				};
			};

			MarkdownReader.prototype.display = function(data) {
				var text = JSON.stringify(data, null, 4);
				$("#display").text(text);
			};

			return MarkdownReader;
		}());

	return MarkdownReader;
});
