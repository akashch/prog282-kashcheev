/**
 * @author Andrey Kashcheev
 */

define(["PagedownSetup", "Markdown", "Editor", 'Utilities'], function(PagedownSetup, Markdown, Editor, utilities) {

	var MarkShow = ( function() {

			var saveMarkdown;
			var converter;
			var inputText = null;
			var fileNameData;
			var editor;

			function MarkShow() {
				$('#homePage').click(function(){
					window.location.href = '/';
				});
				
				$('#savePage').click(savePage);
				var pagedownSetup = new PagedownSetup();
				//converter = pagedownSetup.setupConverter(Markdown);
				editor = pagedownSetup.setupEditor(Markdown);
				converter = editor.getConverter();

				inputText = $("#wmd-input-elf");
				fileNameData = $("#fileName");
				inputText.html("This is the starter text with tweaked editor.\n\n- A\n- B\n");
				
				converter.hooks.chain("preConversion", function(text) {
					saveMarkdown = text;
					return text;
				});
			}


			MarkShow.prototype.getPick = function(event) {
				$.getJSON('/getPick', function(result) {
					fileNameData.html(result.userPick);
					$("#sessionNumber").html("Session: " + result.sessionNumber);
					console.log(JSON.stringify(result));
					inputText.html(result.content);
					editor.refreshPreview();
				});
			};
			
			
			var savePage = function() {
				console.log('savePage called');
				$.ajax({
					url: '/Markdown/savePage',
					data: {
						markdown: saveMarkdown,
						html: converter.makeHtml(saveMarkdown),
						fileName: fileNameData.html()
					}
				}).error = utilities.errorHandler;
			};
			

			return MarkShow;

		}());

	return MarkShow;
});
