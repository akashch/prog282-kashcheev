/**
 * @author Charlie Calvert
 */

require.config({
	paths : {
		"jquery" : "jquery-2.1.1",
		"DefaultReader" : "./Readers/DefaultReader",
		"JsonReader" : "./Readers/JsonReader",
		"MarkdownReader" : "./Readers/MarkdownReader",
		"ReaderFactory" : "./Factories/ReaderFactory",
		"BridgeFactory" : "./Factories/BridgeFactory",
		"ReaderBridge" : "./Bridges/ReaderBridge",
		"FancyReaderBridge" : "./Bridges/FancyReaderBridge",
		"DisplayAddress" : "./Display/DisplayAddress",
		"DisplayFileList": "./Display/DisplayFileList",
		"Markdown" : "./Markdown/Markdown.Converter",
		"Editor" : "./Markdown/Markdown.Editor",
		"Prettify" : "./Markdown/prettify",
        "MarkdownExtra" : "./Markdown/Markdown.Extra"
		
	},
	shim : {
        'Markdown' : {
            exports : 'Markdown'
        },
        'Editor' : {
            deps : [ 'Markdown' ],
            exports : 'Editor'
        },
        "Prettify" : {
            deps : [ 'Markdown', 'Editor' ],
            exports : 'Prettify'
        },
        'MarkdownExtra' : {
            deps : [ 'Markdown', 'Editor', 'Prettify' ],
            exports : 'MarkdownExtra'
       }
    }
});

function endsWith(value, suffix) {
    return value.indexOf(suffix, this.length - suffix.length) !== -1;
}

require([ 'jquery', "Control", "MarkShow", "TinyPubSub", 'MarkdownExtra' ],
function(jq, Control, MarkShow, PubSub, MarkdownExtra) {
    'use strict';
    console.log("Main called");

    $(document).ready(function() {
        if (endsWith(document.URL, "Markdown")) {
            var markShow = new MarkShow();
            markShow.getPick();
        } else {
            var control = new Control();
        };
    });
});
