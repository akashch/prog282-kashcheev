define(["ReaderFactory", "BridgeFactory","FileTypeSorter", "Utilities"], function(ReaderFactory, BridgeFactory, FileTypeSorter, utilities) {

	var Control = ( function() {'use_strict';

			var fancyReader = null;
			var factory = null;
			var fileTypeSorter = null;

			var options = {
				defaultFileName : "public/FileList.json",
				useDefaultFile : true,
			};
			options.readers = ["JsonReader", "MarkdownReader"];
			options.objectType = options.readers[0];
			options.fileName = options.defaultFileName;

			function Control() {
				factory = new ReaderFactory();
				fancyReader = new BridgeFactory().create({
					objectType : "FancyReaderBridge"
				});
				fileTypeSorter = new FileTypeSorter();
				$.subscribe('pageRefresh', function() {
					$("li").click(run);
				});
				
				//$('#showMarkdown').click(showMarkdown);
				run();
			}


			function runReader(options) {
				utilities.displayOptions(options);
				var reader = factory.create(options);
				fancyReader.setReader(reader);
				fancyReader.readFile(options.fileName);
			}

			function run(event) {
				options.useDefaultFile = fileTypeSorter.setFileName(options, event);
				runReader(options);
			}

			return Control;

		}());

	return Control;
}); 