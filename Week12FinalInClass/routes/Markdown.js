/**
 * @author Andrey Kashcheev
 */
var express = require('express');
var router = express.Router();
var fs = require('fs');
var queryMongo = require('./QueryMongo').QueryMongo;
/* GET home page. */
router.get('/', function(req, res) {
  console.log("Markdown called");
  res.render('Markdown', { title: 'Markdown' });
});


router.get('/savePage', function(request, response){
	console.log("Markdown.savePage.called");
	console.log("Filename: " + request.query.fileName);
	console.log("Markdown: " + request.query.markdown);
	var fileName = request.query.fileName;
	var htmlFileName = fileName.substr(0, fileName.lastIndexOf('.'))+ '.html';
	var html = request.query.html;
	fs.writeFile(request.query.fileName, request.query.markdown, function(error, data){
		if(error) throw error;
		//resonse.send({result: "Success"});
	});

	var objectToInsert ={  type:'htmlMarkdown',
							   html: request.query.html,
							   markdown: request.query.markdown,
							   fileName: request.query.fileName
						   };
	fs.writeFile(htmlFileName, html, function(error, data){
		if(error) throw error;
		
		//response.send({result: "Success"});
	});
	console.log(queryMongo);
	queryMongo.insertIntoCollection(response, objectToInsert);
	
});
module.exports = router;
