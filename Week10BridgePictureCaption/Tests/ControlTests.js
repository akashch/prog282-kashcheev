/**
 * @author Andrey Kashcheev
 */

define(["Control", "JsonReader"], function(Control, JsonReader) {'use strict';

	describe("Control Tests", function() {

	var a, b, c, d, f;
	
		beforeEach(function() {
			a = new JsonReader();
			b = new JsonReader();
			c = new JsonReader();
			d = new JsonReader();
			f = [];
		});

		it("proves we can run a test", function() {
			expect(true).toBe(true);
		});

		//Unit Test for Singelton
		it("proves we can run a sanity check that we expect to fail", function() {
			var e = {};
			expect(a === e).toBe(false);
		});

		it("proves we can create a JsonReader01", function() {
			expect(a === b).toBe(true);
		});

		it("proves we can create a JsonReader02", function() {
			expect(a === c).toBe(true);
		});

		it("proves we can run another sanity check", function() {
			expect(a === f).toBe(false);
		});
		//end

		it("proves we can create Control", function() {
			var control = new Control();
			expect(control).toBeTruthy();
		});

		

	});
});
