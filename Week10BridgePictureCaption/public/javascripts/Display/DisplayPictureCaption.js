/**
 * @author Andrey Kashcheev
 */

define(function(require) {
    'use strict';

    var DisplayPictureCaption = (function() {

        function DisplayPictureCaption() {

        }

        DisplayPictureCaption.prototype.display = function(fileList) {
            console.log(fileList);
            for ( var file in fileList) {
                var content = fileList[file].caption;
                var listItem = '<li data="address" ><img src="' + fileList[file].url + '" alt="Smiley face">' + content + '</li>';
                $('#displayList').append(listItem);
            }
     
            $.publish('pageRefresh', { message : "Refreshed Address" });
        };

        return DisplayPictureCaption;
    }());

    return DisplayPictureCaption;

});