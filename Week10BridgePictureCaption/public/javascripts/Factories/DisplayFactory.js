/**
 * @author Andrey Kashcheev
 */

define(["DisplayFileList", "DisplayAddress", "DisplayPictureCaption"], function(DisplayFileList, DisplayAddress, DisplayPictureCaption) {'use strict';'use strict';

	// Define a SailBoat factory constructor function
	function DisplayFactory() {
	}


	DisplayFactory.prototype.product = {};

	// Create a Boat with this function
	DisplayFactory.prototype.create = function(options) {

		switch (options.objectType) {
			case "fileList":
				this.product = new DisplayFileList();
				break;
			case "address":
				this.product = new DisplayAddress();
				break;
			case "pictureCaption":
				this.product = new DisplayPictureCaption();
				break;
			default:
				this.product = new DisplayFileList();
		}

		return this.product;

	};

	return DisplayFactory;
}); 