function square(a){
	return a*a;
}

function calculate(func){
	return func(5);
}

var result2 = square(3);
console.log(result2);

var result = calculate(square);
console.log(result);

